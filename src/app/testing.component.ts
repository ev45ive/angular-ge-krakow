import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-testing',
  template: `
    <p>
      {{message}}
    </p>
    <input type="text" [(ngModel)]="message">
  `,
  styles: []
})
export class TestingComponent implements OnInit {

  message = 'testing works!'

  constructor() { }

  ngOnInit() {
  }

}
