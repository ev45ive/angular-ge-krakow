import { Component, OnInit } from '@angular/core';
import { Playlist } from '../model/playlist';
import { ActivatedRoute, Router } from '../../../node_modules/@angular/router';

@Component({
  selector: 'app-playlists',
  template: `
  <div class="row">
    <div class="col">
    <app-item-list [items]="playlists" 
    [selected]="selected"
    (selectedChange)="select($event)" >
    </app-item-list>
    
    </div>
    <div class="col">
      <app-playlist-details *ngIf="selected" [playlist]="selected"
       (save)="savePlaylist($event)"></app-playlist-details>

       <p *ngIf="!selected" >Nothing to show. Please select</p>
    
    </div>
  </div>
  `,
  styles: []
})
export class PlaylistsComponent implements OnInit {

  select(playlist) {
    this.router.navigate(['/playlists', playlist.id],{
      queryParams:{placki:'YES'}
    })
  }

  selected:Playlist

  constructor(
    private router: Router,
    private route: ActivatedRoute) {

    // const id = parseInt(route.snapshot.paramMap.get('id'))

    route.paramMap.subscribe(paramMap => {
      const id = parseInt(paramMap.get('id'))
      
      const playlist = this.playlists.find(p => p.id == id)

      if (playlist) {
        this.selected = playlist
      }
    })

  }

  savePlaylist(playlist) {
    const index = this.playlists.findIndex(p => p.id == playlist.id)
    if (index !== null) {
      this.playlists.splice(index, 1, playlist)
    }
    this.selected = playlist
  }

  playlists: Playlist[] = [
    {
      id: 123,
      name: 'Angular Hits!',
      favourite: true,
      color: '#ff0000'
    },
    {
      id: 234,
      name: 'Angular Top 20!',
      favourite: true,
      color: '#00ff00'
    },
    {
      id: 345,
      name: 'The BEst Of Angular !',
      favourite: true,
      color: '#0000ff'
    },
  ]



  ngOnInit() {
  }

}
