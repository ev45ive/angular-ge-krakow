import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Playlist } from 'src/app/model/playlist';

@Component({
  selector: 'app-playlist-details',
  template: `
  <div [ngSwitch]="mode">
    <div *ngSwitchDefault>
      <dl>
        <dt>Name:</dt>
        <dd>{{playlist.name}}</dd>

        <dt>Favourite</dt>
        <dd>{{playlist.favourite? 'Yes' : 'No'}}</dd>

        <dt>Color</dt>
        <dd [style.background-color]="playlist.color">{{playlist.color}}</dd>
      </dl>
      <!-- input[type=button].btn.btn-info -->
      <input type="button" class="btn btn-info" value="Edit" (click)="edit()">
    </div>

    <form *ngSwitchCase=" 'edit' " #formRef="ngForm" (submit)="save(formRef)">
      <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control" #nameRef="ngModel" [ngModel]="playlist.name" name="name">
      </div>

      <div class="form-group">
        <label>Favourite</label>
        <input type="checkbox" [ngModel]="playlist.favourite" name="favourite">
      </div>

      <div class="form-group">
        <label>Color</label>
        <input type="color"  [ngModel]="playlist.color" name="color">
      </div>
      <input type="button" class="btn btn-danger" value="Cancel" (click)="cancel()">
      <input type="submit" class="btn btn-success" value="Save" >
    </form>
  </div>

  `,
  styles: []
})
export class PlaylistDetailsComponent implements OnInit {

  @Input()
  playlist:Playlist

  mode: 'show' | 'edit' = 'show'

  edit(){
    this.mode = 'edit'
  }

  cancel(){
    this.mode = 'show'
  }

  @Output('save')
  saveEmitter = new EventEmitter()

  save(formRef){
    const playlist = {
      ...this.playlist,
      ...formRef.value
    }
    this.saveEmitter.emit(playlist)
    this.mode = 'show'
  }

  constructor() { }

  ngOnInit() {
  }

}
