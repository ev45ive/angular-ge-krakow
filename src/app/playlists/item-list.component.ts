import { Component, OnInit, ViewEncapsulation, Input, EventEmitter, Output } from '@angular/core';
import { Playlist } from '../model/playlist';
import { NgForOfContext, NgForOf } from '../../../node_modules/@angular/common';
NgForOfContext
NgForOf
@Component({
  selector: 'app-item-list',
  template: `
  <!-- .list-group>.list-group-item.list-group-item-action{Text} -->

    <div class="list-group">
      <div *ngFor="let playlist of playlists; index as i; trackBy trackFn" 
            class="list-group-item list-group-item-action"
            [class.active]=" selected?.id == playlist.id "
            (click)="select(playlist)"
            [ngClass]="{
              placki:true,
              malinowe: false 
            }"
            >
        {{i+1}}. {{playlist.name}}
      </div>
    </div>
    {{selected?.name}}
  `,
  /* inputs:[
    'playlists:items'
  ], */
  encapsulation: ViewEncapsulation.Emulated,
  styles: [`
    :host-context(.bordered){
      border:1px solid black;
      display:block;
    }
    :host(.colored) p{
      color: hotpink;
    }
  `]
})
export class ItemListComponent implements OnInit {

  @Input('items')
  playlists: Playlist[] = []

  @Input()
  selected: Playlist

  @Output()
  selectedChange = new EventEmitter<Playlist>()

  select(playlist: Playlist) {
    this.selectedChange.emit(playlist)
  }

  trackFn(i, item) {
    return item.id
  }

  constructor() {
    this.selected = this.playlists[0]
  }

  ngOnInit() {
    console.log(this.playlists)
  }

}
