import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestingComponent } from './testing.component';
import { By } from '@angular/platform-browser';
import { FormsModule } from '../../node_modules/@angular/forms';

fdescribe('TestingComponent', () => {
  let component: TestingComponent;
  let fixture: ComponentFixture<TestingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TestingComponent],
      imports: [FormsModule],
      providers: []
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render text', () => {
    const p = fixture.debugElement.query(By.css('p'))
    expect(p.nativeElement.innerText).toEqual('testing works!')
  });

  it('should render text form model', () => {
    component.message = "Placki"
    
    fixture.detectChanges()

    const p = fixture.debugElement.query(By.css('p'))
    expect(p.nativeElement.innerText).toEqual('Placki')
  });

  it('should update message from input', () => {
    const input = fixture.debugElement.query(By.css('input'))

    input.nativeElement.value = "Placki"
    input.triggerEventHandler('input',{ target: input.nativeElement})

    expect(component.message).toEqual('Placki')
  });

  it('should render input value form model', async(() => {
    const input = fixture.debugElement.query(By.css('input'))
    component.message = "Placki"
    
    fixture.detectChanges()

    fixture.whenRenderingDone().then(()=>{

      const p = fixture.debugElement.query(By.css('p'))
      expect(input.nativeElement.value).toEqual('Placki')
    })

  }));
});
