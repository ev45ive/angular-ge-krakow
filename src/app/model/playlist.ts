interface Entity{
    id: number;
    name: string;
}

export interface Playlist {
    id: number;
    name: string;
    favourite: boolean;
    color: string;
    // tracks: Array<Track>;
    // tracks: Track[] | undefined
    /**
     * Playlist tracks
     * @author Placki! 
     */
    tracks?: Track[] 
}

export interface Track extends Entity{
  /*   id: number;
    name: string; */
}

/* export class Playlist{
    constructor(
        public name:string,
        public favourite:boolean
    ){}
}

var p:Playlist = {
    name:'',favourite:true
}

 */
