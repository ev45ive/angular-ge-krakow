import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ApplicationRef } from '@angular/core';

import { AppComponent } from './app.component';
import { PlaylistsModule } from './playlists/playlists.module';
import { MusicModule } from './music/music.module';
import { SecurityModule } from './security/security.module';
import { SecurityService } from './security/security.service';
import { AppRoutingModule } from './app-routing.module';
import { TestingComponent } from './testing.component';
import { FormsModule } from '../../node_modules/@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    TestingComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    PlaylistsModule,
    MusicModule,
    SecurityModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private security:SecurityService, app:ApplicationRef){
  

    this.security.getToken()
  }
 }
