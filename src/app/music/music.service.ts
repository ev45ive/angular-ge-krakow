import { Injectable, Inject, InjectionToken } from '@angular/core';
import { Album } from 'src/app/model/album';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { SecurityService } from '../security/security.service';

// import "rxjs/Rx"
// import "rxjs/add/operator/map"
import { map, catchError, switchMap, startWith } from 'rxjs/operators'
import { of, throwError, Subject, BehaviorSubject } from 'rxjs';

export const SEARCH_URL = new InjectionToken('URL for albums search API')

interface AlbumsResponse {
  albums: {
    items: Album[]
  }
}

@Injectable({
  providedIn: 'root'
})
export class MusicService {

  queries = new BehaviorSubject<string>('alice')
  albums = new BehaviorSubject<Album[]>([])

  constructor(
    @Inject(SEARCH_URL) private api_url,
    private http: HttpClient,
    private security: SecurityService
  ) {

    this.queries.pipe(
      map(query => ({
        type: 'album',
        q: query
      })),
      switchMap(params => this.http.get<AlbumsResponse>(this.api_url, {
        headers: {
          Authorization: 'Bearer ' + this.security.getToken()
        },
        params
      })),
      map(response => response.albums.items),
      catchError(err => {
        if (err instanceof HttpErrorResponse && err.status == 401) {
          this.security.authorize()
          return throwError(new Error('Access Denied'))
        }
        return of([])
      })
    ).subscribe(albums => this.albums.next(albums))
  }

  search(query) {
    this.queries.next(query)
  }

  getAlbums() {
    return this.albums
    // .pipe(startWith(this.albumCache))
  }


}
