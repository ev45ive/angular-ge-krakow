import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, AsyncValidator, AsyncValidatorFn, ValidatorFn, ValidationErrors, AbstractControl } from '@angular/forms';
import { distinctUntilChanged, map, filter, debounceTime, withLatestFrom } from 'rxjs/operators';
import { Observable, Observer } from 'rxjs';


//https://getbootstrap.com/docs/4.1/components/input-group/#button-addons

@Component({
  selector: 'app-search-form',
  template: `
  <form class="form-group mb-3" [formGroup]="queryForm">
    <input type="text" class="form-control" placeholder="Search" formControlName="query">
    <div *ngIf="queryForm.pending">Please wait...</div>

    <div class="errors" *ngIf="queryForm.get('query').touched || queryForm.get('query').dirty">
    
      <div *ngIf="queryForm.get('query').hasError('required')">
        Field is required
      </div>
      <div *ngIf="queryForm.get('query').getError('minlength') as error">
        Field should have at least {{error.requiredLength}} characters
      </div>
      <div *ngIf="queryForm.get('query').getError('censor') as error">
        Field should not contain '{{error.badword}}'
      </div>
    </div>
  </form>
  `,
  styles: [`
    form .ng-invalid.ng-touched,
    form .ng-invalid.ng-dirty{
      border:2px solid red;
    }
    form .ng-invalid ~ .errors {
      color:red;
    }
  `]
})
export class SearchFormComponent implements OnInit {

  @Output()
  queryChange = new EventEmitter<string>()

  queryForm: FormGroup

  constructor() {

    const censor = (badword): ValidatorFn =>
      (control: AbstractControl): ValidationErrors | null => {
        const hasError = control.value.includes(badword)

        return hasError ? {
          'censor': {
            badword
          }
        } : null
      }

    const asyncCensor = (badword): AsyncValidatorFn => (control: AbstractControl) => {
      // this.http.get(...).pipe(map(response => validationErrors))

      return Observable.create((observer: Observer<ValidationErrors | null>) => {

        const handler = setTimeout(() => {
          const hasError = control.value.includes(badword)
          observer.next(hasError ? {
            'censor': {
              badword
            }
          } : null)
          observer.complete()
        }, 2000)

        return () => {
          clearTimeout(handler)
        }
      })
    }

    this.queryForm = new FormGroup({
      'query': new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        // censor('batman')
      ], [
          asyncCensor('batman')
        ]),
    })

    const status$ = this.queryForm.get('query').statusChanges
    const query$ = this.queryForm.get('query').valueChanges.pipe(
      debounceTime(400),
      map(query => query.trim()),
      distinctUntilChanged(),
      filter(query => query.length >= 3)
    )

    const valid$ = status$.pipe(
      filter(status => status == "VALID")
    )

    valid$.pipe(
      withLatestFrom(query$, (valid, query) => query)
    )
      .subscribe(query => {
        this.search(query)
      })

    // console.log(this.queryForm)
  }

  search(query) {
    this.queryChange.emit(query)
  }


  ngOnInit() {
  }

}
