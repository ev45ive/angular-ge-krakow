import { Component, OnInit, Inject } from '@angular/core';
import { Album } from 'src/app/model/album';
import { MusicService } from './music.service';
import { Subscription, Observable } from '../../../node_modules/rxjs';
import { takeUntil } from '../../../node_modules/rxjs/operators';

@Component({
  selector: 'app-music-search',
  template: `
    <div class="row">
      <div class="col">
        <app-search-form (queryChange)="search($event)"></app-search-form>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <ng-container *ngIf="albums | async as albumsList">
          Found {{albumsList.length}} albums
          <app-albums-grid [albums]="albumsList"></app-albums-grid>
        </ng-container>
      </div>
    </div>
  `,
  styles: []
})
export class MusicSearchComponent implements OnInit {

  albums: Observable<Album[]>
  error: string

  constructor(private musicService: MusicService) {
    this.albums = this.musicService.getAlbums()
  }

  search(query) {
    this.musicService.search(query)
  }

  ngOnInit() { }


}
