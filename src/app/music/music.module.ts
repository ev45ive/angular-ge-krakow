import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MusicSearchComponent } from './music-search.component';
import { SearchFormComponent } from './search-form.component';
import { AlbumsGridComponent } from './albums-grid.component';
import { AlbumItemComponent } from './album-item.component';
import { SEARCH_URL } from './music.service';
import { environment } from 'src/environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [
    MusicSearchComponent,
    SearchFormComponent,
    AlbumsGridComponent,
    AlbumItemComponent
  ],
  exports: [MusicSearchComponent],
  providers: [
    {
      provide: SEARCH_URL,
      useValue: environment.search_url
    },
    /* {
      provide: MusicService,
      useFactory: (api_url) => {
        return new MusicService(api_url)
      },
      deps:['SEARCH_URL']
    },
    {
      provide: AbstractMusicService,
      useClass: SpotifyMusicService,
      // deps:['SEARCH_URL']
    },
    MusicService */
  ]
})
export class MusicModule { }
