import { Component, OnInit, Input } from '@angular/core';
import { Album } from '../model/album';

@Component({
  selector: 'app-albums-grid',
  template: `
    <div class="card-group">
      <!-- <div class="card" app-album-item></div> -->
      <app-album-item class="card" *ngFor="let album of albums" [album]="album">
      </app-album-item>    
    </div>
  `,
  styles: [`
    .card{
      flex: 0 0 25%;
    }
  `]
})
export class AlbumsGridComponent implements OnInit {

  @Input()
  albums:Album[]

  constructor() { }

  ngOnInit() {
  }

}
